<?php
	session_start();
	
	require_once 'MathCaptcha.php';
	$form_type = $_REQUEST['form_type'];
	
	$cpa = new MathCaptcha($form_type);

	
	if( isset($_REQUEST['submit']) ){
		$captcha_val = $_REQUEST['captcha'];

		if( $cpa->validate($captcha_val) ){
			
			switch ($form_type) {
				case 'partner-form':
					$url = "https://docs.google.com/forms/d/e/1FAIpQLScR2whD0pga_XR415msZMFDc6NdiD8A2v59gb-2E5MNnawtiw/formResponse";
					list($gfRequest, $gfResult) = doPost($url, $_POST);
					echo curl_getinfo($gfRequest, CURLINFO_HTTP_CODE);
					break;
				case 'support-form':
					$url = "https://docs.google.com/forms/d/e/1FAIpQLSf9N345AWpfqazl0h_gvu0U9nAC0NfFL-cpp-G5WOnSneFWkw/formResponse";
					list($gfRequest, $gfResult) = doPost($url, $_POST);
					echo curl_getinfo($gfRequest, CURLINFO_HTTP_CODE);
					break;
				case 'contact-form':
					$url = "https://docs.google.com/forms/d/e/1FAIpQLSeAJCCruaASyi0z17mMJujLsCy_puKPx0hHv2tR4kSsN3i3Tg/formResponse";
					list($gfRequest, $gfResult) = doPost($url, $_POST);
					echo curl_getinfo($gfRequest, CURLINFO_HTTP_CODE);
					break;
				default:
					# code...
					break;
			}
		}else{
			header('Content-type: application/json');
			print_r(json_encode(array('captcha_res'=>'incorrect')));
		}
		$cpa->reset_captcha();
	}
	else
	{
		// Initialize captcha
		$cpa->sums_only_please();
		$cpa->reset_captcha();
		echo $cpa->get_captcha_text('Please verify that you\'re Human. Result of <i><b>{operand1}</i> {operator} <i>{operand2}</b></i> is?');
	}





	function doPost($url, $fields){
		//url-ify the data for the POST
		$fields_string="";
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		$fields_string = rtrim($fields_string,'&');

		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST,count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);

		//execute post
		$result = curl_exec($ch);
		return array($ch,  $result);
	}
?>