$(document).ready(function() {


    $('body').append('<div class="modal fade" id="customalert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">\
      <div class="modal-dialog" role="document">\
        <div class="modal-content">\
            <div class="modal-header">\
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\
            </div>\
          <div class="modal-body">\
            sdgd\
          </div>\
          \
        </div>\
      </div>\
    </div>');

    $("#product-modal").load("/product.html", function(){
        $('#product-submit').on('click', function(){
            $('#product-coming-soon').modal('hide');
            customalert('Thanks a ton! We’ll keep you updated.');
            $('#product-email').val('');
        });
    });

    $('[href="/products"]').on('click', function(){
        $('#product-coming-soon').modal('show');
        return false;
    })

	$(window).scroll(function() {
        var height = $(window).scrollTop();
        var headerHeight = 0;
        header = $('.header-con');
        if(height  > headerHeight) {
        	var winwidth = $(window).width();
            header.css('width',winwidth+"px")
            header.css({position:"fixed", top:0});
            $('body').css({'padding-top': header.height() + "px"});
        }
    });


    hideFormTitles();
  
    $.stellar(
            {
                horizontalScrolling: false,
            }
    );


    var partnerexp = $('#itemsyousell');
    if(partnerexp){
        partnerexp.hide();
    }
    //For google form submit
    $('form').each(function(){
        var form_type=$(this).find('input[name="form_type"]').val();
        formCaptcha(form_type);
    });

    $("#mG61Hd #support-problem-type").on("change", function(){
        $(this.form).find('input[name="entry.482627583"]').val($(this).val());
        if($(this).val() !== " "){
            $(this.form).find("textarea[name='entry.922382441']").css('display','block');
        }
        else{
            $(this.form).find("textarea[name='entry.922382441']").css('display','none');
        }
    });
    $("#mG61Hd #reachus-requirement").on("change", function(){
        $(this.form).find('input[name="entry.840488049"]').val($(this).val());
    });
    $("#mG61Hd #reachus-purpose").on("change", function(){
        $(this.form).find('input[name="entry.538114459"]').val($(this).val());
    });

    $("#mG61Hd #partners-experience-yesno").on("change", function(){
        $(this.form).find('input[name="entry.1778174561"]').val($(this).val());
        $(this.form).parsley().destroy();
        if($(this).val()=="Yes"){
            $('#itemsyousell').show().attr('required','true').attr('data-parsley-required','true');

        }
        else{
            $('#itemsyousell').hide().attr('required',null).attr('data-parsley-required',null);
        }
    });


   // product-page-onload-show-popup
    $('.launch-soon-btn').trigger('click');


});

    function formCaptcha(formName){
        $.post('php/form_submit.php', {'form_type': formName} )
            .done(function(data){
                setCaptchaText(formName, data);
            })
            .fail(function(){setCaptchaText(formName, 'Please reload this page')});
    }

    function setCaptchaText(formName, text){
        $('#'+formName+' form .captchalabel').html(text);
    }

    function hideFormTitles(){
        $('[title]').mouseover(function () {
            $this = $(this);
            $this.data('title', $this.attr('title'));
            // Using null here wouldn't work in IE, but empty string will work just fine.
            $this.attr('title', '');
        }).mouseout(function () {
            $this = $(this);
            $this.attr('title', $this.data('title'));
        });
    }

    function customalert(text){
        $('#customalert').find('.modal-body').text(text);
        $('#customalert').modal('show');

    }

    function google_form_submit(element){
        form = element.form;
        var isValid = $(form).parsley({
            priorityEnabled: true,
        }).validate();
        if(!isValid){
            return false;
        }
        var form_type=$(form).find('input[name="form_type"]').val();

        var data = $(form).serialize();
        $.post('php/form_submit.php',data+'&submit=1', 'json')
            .done(function(data){
                if(data.captcha_res == "incorrect"){
                    var captchaElement = $(form).find('input[name="captcha"]').parsley();
                    window.ParsleyUI.addError(captchaElement, "invalidCaptcha", 'Please enter a valid result');
                    $(form).find('input[name="captcha"]').focus();
                    setTimeout(function(){window.ParsleyUI.removeError(captchaElement, "invalidCaptcha");} , 4000);
                }
                else{
                    if(form_type=="support-form"){
                        $('#support-form-modal').modal('hide');
                        $('.quantumWizTextinputPapertextareaInput').val('');
                        $('.quantumWizTextinputPaperinputInput').val('');
                        $('#support-problem-type').val(' ');

                        customalert("We've received support your request. We'll contact you soon. Thank you!"); 
                        
                    }
                    else if(form_type=="partner-form"){
                        $('#channel-partner-form-modal').modal('hide');
                        var partnerFormOpenButton = $('.partner-join-btn.button-sm');
                        if(partnerFormOpenButton){
                            $(partnerFormOpenButton).click();
                        }
                        
                        $('.quantumWizTextinputPapertextareaInput').val('');
                        $('.quantumWizTextinputPaperinputInput').val('');
                        $("#mG61Hd #partners-experience-yesno").val('');
                        customalert("Thanks very much indeed for showing interest in us. Our channel consultants will catchup with you soon."); 
                    }
                    else if(form_type=="contact-form"){
                        
                        $('.quantumWizTextinputPapertextareaInput').val('');
                        $('.quantumWizTextinputPaperinputInput').val('');
                        $('#reachus-form-modal').modal('hide');

                        customalert("We've received your request. We'll contact you soon. Thank you!"); 
                    }
                }
            })
            .fail(function(){
                customalert("We're sorry. Something unexpected has happened."); 
            })
            .always(function(){
                formCaptcha(form_type);
                $(form).find('input[name="captcha"]').val('');
            });
    }